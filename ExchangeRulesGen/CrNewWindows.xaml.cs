﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для CrNewWindows.xaml
    /// </summary>
    public partial class CrNewWindows : MetroWindow
    {

        public CrNewWindows()
        {
            InitializeComponent();
            frame.Navigate(new PolicyPage(this));
        }

        public void SetTitlePage(string title)
        {

        }

        private async void button_upload_Click(object sender, RoutedEventArgs e)
        {
            //await DialogManager.ShowMessageAsync(this,
            //         "Ошибка", "Функционал отключен, заливать теперь вручную!");
            //return;
            ////
            //if (listView.SelectedIndex == -1)
            //{
            //    await DialogManager.ShowMessageAsync(this,
            //        "Ошибка", "Для начала выберите необходимый элемент!");
            //    return;
            //}
            

            //LoginDialogData result = await this.ShowLoginAsync("Аутентификация", "Введите учетные данные", new LoginDialogSettings { ColorScheme = this.MetroDialogOptions.ColorScheme });
            //if (result == null)
            //{
            //    //User pressed cancel
            //    return;
            //}
            //else
            //{
            //    string pathfull = ExportPolicy(true);
            //    //MessageDialogResult messageResult = await this.ShowMessageAsync("Authentication Information", String.Format("Username: {0}\nPassword: {1}", result.Username, result.Password));


            //    string import = Properties.Resources.scr2;
            //    //string runps = string.Format(import, "1z", "2z", "3z");
            //    string repps = import.Replace("{0}", result.Username);
            //    repps = repps.Replace("{1}", result.Password);
            //    repps = repps.Replace("{2}", pathfull);
            //    repps = repps.Replace("{3}", DataStatic.PolicesList[listView.SelectedIndex].Name);
            //    repps = repps.Replace("{4}", DataStatic.PolicesList[listView.SelectedIndex].RulesList[0].Name);
            //    repps = repps;
            //    string temp = System.IO.Path.GetTempPath();
            //    string ps1Path = temp + "\\scrupload.ps1";
            //    File.WriteAllText(ps1Path, repps, Encoding.Unicode);
            //    Func.PSCommandOrScriptFromCmd("Set-ExecutionPolicy Unrestricted");
            //    string outp = Func.PSCommandOrScriptFromCmd(ps1Path);
            //    await DialogManager.ShowMessageAsync(this,
            //        "Результат", outp);
                
            //}
        }



        

        private void settingsBut_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow setw = new SettingsWindow();
            setw.ShowDialog();
        }

        private void PART_BackButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.textBlock1.Text = ExchangeRulesGen.Properties.Resources.Список_политик;
                this.groupBox.Header = ExchangeRulesGen.Properties.Resources.Список_политик;
                this.separator1.Margin = new Thickness(155, 110, 10, 0);
                frame.GoBack();
            }
            catch (Exception)
            {

            }
            
        }


    }
}
