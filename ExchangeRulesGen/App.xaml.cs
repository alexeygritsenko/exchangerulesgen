﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {

    private void Application_Startup ( object sender, StartupEventArgs e )
    {
      //Show the language select dialog
      System.Windows.ShutdownMode sm = this.ShutdownMode ;
      this.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown ;
      MultiLang.SelectLanguage sl = new MultiLang.SelectLanguage() ;
      sl.LoadSettingsAndShow() ;
      this.ShutdownMode = sm ;
    }

    }
}
