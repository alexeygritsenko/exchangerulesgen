﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRulesGen
{
    public class PolicysForLW
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public static class DataStatic
    {
        //public static Rule rule = null;
        //public static List<Rule> RulesList = null;
        public static List<Policy> PolicesList = new List<Policy>();
        //public static Policy policy = null;
    }

    public static class AuthTmp
    {
        public static string login = null;
        public static string pass = null;
    }

    public class Policy
    {
        public string Name { get; set; }
        public string Descr { get; set; }
        public Rule ruleone = null;
        //public List<Rule> RulesList = null;
    }


        public class Criteria
    {
        //название / тип / уровень конф. /ключевое?
        public string Name { get; set; }
        public string TypeS { get; set; }
        public bool Mainkey { get; set; }
        public string Regex { get; set; }
        public List<string> Dic { get; set; }
        //далее для редактирования
        public int Len { get; set; }
        public int LenMax { get; set; }
        public string DpdopustimCifri { get; set; }
        public bool[] CheckBoxsCecks { get; set; }
        public string AnySimb { get; set; }
        public string WordForTwoType { get; set; }
    }

    public class User
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string Mail { get; set; }


    }

    public class Rule
    {
        public string Name { get; set; }

        public string Descr { get; set; }

        public List<Criteria> CriteriaList = new List<Criteria>();


    }
}
