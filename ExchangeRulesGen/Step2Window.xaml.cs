﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для Step2Window.xaml
    /// </summary>
    public partial class Step2Window : MetroWindow
    {
        Rule rule;
        Criteria CritEdit = null;
        int CriteriaIndexGlobal = -1;
        List<string> lines = null;
        public Step2Window()
        {
            InitializeComponent();
        }

        public Step2Window(ref Rule rule)
        {
            InitializeComponent();
            this.rule = rule;
            
        }

        public Step2Window(ref Rule rule, int CriteriaIndex)
        {
            //режим редактирования
            InitializeComponent();
            this.rule = rule;
            this.CritEdit = rule.CriteriaList[CriteriaIndex];
            CriteriaIndexGlobal = CriteriaIndex;

            if (CritEdit.TypeS == ExchangeRulesGen.Properties.Resources.число)
            {
                tabControl1.SelectedIndex = 0;
                 checkBox.IsChecked = CritEdit.CheckBoxsCecks[0];
                 checkBox1.IsChecked = CritEdit.CheckBoxsCecks[1];
                 checkBox2.IsChecked = CritEdit.CheckBoxsCecks[2];
                 checkBox3.IsChecked = CritEdit.CheckBoxsCecks[3];
                 checkBox4.IsChecked = CritEdit.CheckBoxsCecks[4];
                anySymbTB.Text = CritEdit.AnySimb;
                nameCrTB.Text = CritEdit.Name;
                len_numUpDown.Value = CritEdit.Len;
                len_numUpDown_Max.Value = CritEdit.LenMax;
            }
            if (CritEdit.TypeS == ExchangeRulesGen.Properties.Resources.слово)
            {
                tabControl1.SelectedIndex = 1;
                wordTB.Text = CritEdit.Regex;
            }
            if (CritEdit.TypeS == ExchangeRulesGen.Properties.Resources.словарь)
            {
                tabControl1.SelectedIndex = 2;
                foreach (var item in CritEdit.Dic)
                {
                    wordsDicTB.Text += item + Environment.NewLine;
                }
                
            }
            comboBox.SelectedIndex = tabControl1.SelectedIndex;
        }

        private void done_button_Click(object sender, RoutedEventArgs e)
        {
            if (CriteriaIndexGlobal != -1)
                rule.CriteriaList.RemoveAt(CriteriaIndexGlobal);
            //
            string TypeSTmp = "";
            string regex = "";
            int len = 1;
            int lenmax = 0;
            if (tabControl1.SelectedIndex == 0)
            {
                //число
                TypeSTmp = ExchangeRulesGen.Properties.Resources.число;
                len = (int)len_numUpDown.Value;
                if (len_numUpDown_Max.IsEnabled)
                    lenmax = (int)len_numUpDown_Max.Value;
                string dpdopustimCifri = dopustimCifriTB.Text;
                List<string> charList = new List<string>();
                //если есть минус то его последним
                if ((bool)checkBox.IsChecked)
                    charList.Add("\\t");
                if ((bool)checkBox1.IsChecked)
                    charList.Add(";");
                if ((bool)checkBox2.IsChecked)
                    charList.Add(",");
                if ((bool)checkBox3.IsChecked)
                    charList.Add(" ");
                if ((bool)checkBox4.IsChecked)
                    charList.Add(anySymbTB.Text);
                else
                    anySymbTB.Text = ""; //for security
                //генерим регех
                string syms = "";
                foreach (var item in charList)
                {
                    syms += item;
                }
                if (len_numUpDown_Max.IsEnabled)
                    regex = @"[" + dpdopustimCifri + syms + "]{" + len + "," + lenmax + "}";
                else
                    regex = @"[" + dpdopustimCifri + syms + "]{" + len + "," + ((len * 2) - 1) + "}";
                regex = regex;

            }
            if (tabControl1.SelectedIndex == 1)
            {
                //слово
                TypeSTmp = ExchangeRulesGen.Properties.Resources.слово;
                regex = wordTB.Text;
            }

            if (tabControl1.SelectedIndex == 2)
            {
                //слово
                TypeSTmp = ExchangeRulesGen.Properties.Resources.словарь;
                lines = new List<string>();
                int lineCount = wordsDicTB.LineCount;

                for (int line = 0; line < lineCount; line++)
                {
                    string linetext = wordsDicTB.GetLineText(line);
                    if (!string.IsNullOrEmpty(linetext))
                    {
                        linetext = linetext.Replace("\r", "");
                        linetext = linetext.Replace("\n", "");
                        if (!string.IsNullOrEmpty(linetext))
                            lines.Add(linetext);
                    }
                }

                
                //regex = wordsDicTB.Text;
            }

            if (!string.IsNullOrEmpty(regex) || lines != null)
            {
                bool MainKeyTmp = false;
                if (CritEdit != null)
                    MainKeyTmp = CritEdit.Mainkey;

                if (tabControl1.SelectedIndex == 0)
                {
                    bool[] checkBoxsCecks = new bool[5];
                    checkBoxsCecks[0] = (bool)checkBox.IsChecked;
                    checkBoxsCecks[1] = (bool)checkBox1.IsChecked;
                    checkBoxsCecks[2] = (bool)checkBox2.IsChecked;
                    checkBoxsCecks[3] = (bool)checkBox3.IsChecked;
                    checkBoxsCecks[4] = (bool)checkBox4.IsChecked;
                    string name = dopustimCifriTB.Text;
                    if ((bool)checkBox.IsChecked)
                        name += ", " + checkBox.Content;
                    if ((bool)checkBox1.IsChecked)
                        name += ", " + checkBox1.Content;
                    if ((bool)checkBox2.IsChecked)
                        name += ", " + checkBox2.Content;
                    if ((bool)checkBox3.IsChecked)
                        name += ", " + checkBox3.Content;
                    if ((bool)checkBox4.IsChecked)
                        name += ", " + checkBox4.Content;
                    name += ", " + ExchangeRulesGen.Properties.Resources.от + len + ExchangeRulesGen.Properties.Resources._до + lenmax;

                    rule.CriteriaList.Add(new Criteria() { Name = name, Mainkey = MainKeyTmp, TypeS = TypeSTmp, Regex = regex, AnySimb = anySymbTB.Text, CheckBoxsCecks = checkBoxsCecks, Len = len, LenMax=lenmax });
                }
                if (tabControl1.SelectedIndex == 1)
                {
                    string name = wordTB.Text;
                    rule.CriteriaList.Add(new Criteria() { Name = name, Mainkey = MainKeyTmp, TypeS = TypeSTmp, Regex = regex, WordForTwoType = wordTB.Text });

                }
                if (tabControl1.SelectedIndex == 2)
                {
                    
                    var crit = new Criteria() { Name = nameCrTB.Text, Mainkey = MainKeyTmp, TypeS = TypeSTmp };
                    crit.Dic = new List<string>();
                    crit.Dic = lines;
                    rule.CriteriaList.Add(crit);
                    string name = "";
                    foreach (var item in crit.Dic)
                    {
                        if (name == "")
                        name += item;
                        else name += ", " + item;
                    }
                    crit.Name = name;
                }
                this.Close();
            }
        }

        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void checkBoxLenMax_Click(object sender, RoutedEventArgs e)
        {
            len_numUpDown_Max.IsEnabled = checkBoxLenMax.IsChecked.Value;
        }

        private void checkBox4_Click(object sender, RoutedEventArgs e)
        {
            anySymbTB.IsEnabled = checkBoxLenMax.IsChecked.Value;
        }

        private void len_numUpDown_ValueIncremented(object sender, NumericUpDownChangedRoutedEventArgs args)
        {
            if (len_numUpDown_Max.Value <= len_numUpDown.Value)
                len_numUpDown_Max.Value = len_numUpDown.Value + 1;
        }

        private void len_numUpDown_Max_ValueDecremented(object sender, NumericUpDownChangedRoutedEventArgs args)
        {
            if (len_numUpDown.Value <= len_numUpDown_Max.Value)
                len_numUpDown.Value = len_numUpDown_Max.Value - 1;
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabControl1.SelectedIndex = comboBox.SelectedIndex;
        }
    }
}
