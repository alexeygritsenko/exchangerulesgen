﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : MetroWindow
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        private void LoadSettings()
        {
            nameTB.Text = Properties.Settings.Default.PublisherName;
            idPubTB.Text = Properties.Settings.Default.PublisherId;
            exportPathTB.Text = ExchangeRulesGen.Properties.Resources._ВашиДокументы_Exchange_DLP_Ex;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSettings();
        }

        private void buttonSettCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonSettSave_Click(object sender, RoutedEventArgs e)
        {
            if (nameTB.Text == "" || idPubTB.Text == "")
            {
                 DialogManager.ShowMessageAsync(this,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Заполните_все_поля);
                return;
            }
            Properties.Settings.Default.PublisherName = nameTB.Text;
            Properties.Settings.Default.PublisherId = idPubTB.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void buttonGenGUIDPublisher_Click(object sender, RoutedEventArgs e)
        {
            idPubTB.Text = Func.GenGUID();
        }

        
    }
}
