﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для PolicyPage.xaml
    /// </summary>
    public partial class PolicyPage : Page
    {
        MetroWindow winmain = null;
        bool firstStart = true;
        public PolicyPage()
        {
            InitializeComponent();

        }

        public PolicyPage(MetroWindow winmain)
        {
            InitializeComponent();
            this.winmain = winmain;
        }

        private string ExportPolicy(bool quiet = false)
        {
            var rule = DataStatic.PolicesList[listView.SelectedIndex].ruleone;

            string md = Environment.GetFolderPath(Environment.SpecialFolder.Personal);//путь к Документам
            string md2 = md + "\\Exchange DLP Export Folder";
            if (Directory.Exists(md2) == false)
            {
                Directory.CreateDirectory(md2);
            }
            //тут выбераем куда сохранить
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Xml DLP Policy file (*.xml)|*.xml";
            saveFileDialog.InitialDirectory = md2;
            string datetime = DateTime.Now.ToString("dd_MM_yyyy-hh_mm_ss");
            saveFileDialog.FileName = DataStatic.PolicesList[listView.SelectedIndex].Name + "_" + datetime;
            if (saveFileDialog.ShowDialog() == true)
            {
                //string md3 = md2 + "\\" + DataStatic.PolicesList[listView.SelectedIndex].Name;
                //if (Directory.Exists(md3) == false)
                //{
                //    Directory.CreateDirectory(md3);
                //}

                string filenamefullpath = Func.GenXmlFordlpPolicyTemplatesWithOneRule(rule, saveFileDialog.FileName, datetime, DataStatic.PolicesList[listView.SelectedIndex].Name, DataStatic.PolicesList[listView.SelectedIndex].Descr);
                if (!quiet)
                    DialogManager.ShowMessageAsync(winmain,
                            ExchangeRulesGen.Properties.Resources.Готово, ExchangeRulesGen.Properties.Resources.Экспортированно + Environment.NewLine +
                            filenamefullpath);

                return filenamefullpath;
            }
            return "";
        }

        private void button_next_Click(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrEmpty(nameTB.Text)) return;
            //Rule rule = new Rule { Name = nameTB.Text, Descr = DescrTB.Text };
            //DataStatic.rule = rule;
            //Step2Window st2 = new Step2Window(rule);
            //st2.ShowDialog();
        }

        private async void Create_button_Pol_Click(object sender, RoutedEventArgs e)
        {
            //var result = await winmain.ShowInputAsync("Название", "Задайте название новой политики");

            //if (result == null) //user pressed cancel
            //    return;

            CrWindow win = new CrWindow(winmain);           
            ((CrNewWindows)winmain).textBlock1.Text = ExchangeRulesGen.Properties.Resources.Политика_Правило;
            ((CrNewWindows)winmain).groupBox.Header = ExchangeRulesGen.Properties.Resources.Политика_Правило;
            ((CrNewWindows)winmain).separator1.Margin = new Thickness(175, 110, 10, 0);
            NavigationService.Navigate(win);

            //win.Closing -= Winmain_Closing;
            //win.Closing += Winmain_Closing;
            //win.ShowDialog();

        }

        private void Winmain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Thread.Sleep(100);
            try
            {

                //if (DataStatic.policy != null)
                //{
                //    List<string> nlist = new List<string>();
                //    foreach (var item in DataStatic.PolicesList)
                //    {
                //        nlist.Add(item.Name);
                //    }
                //    if (!DataStatic.PolicesList.Contains(DataStatic.policy) && !nlist.Contains(DataStatic.policy.Name))
                //    {
                //        if (DataStatic.policy.RulesList == null) DataStatic.policy.RulesList = new List<Rule>();
                //        DataStatic.policy.RulesList.Add(DataStatic.rule); //new
                //        DataStatic.PolicesList.Add(DataStatic.policy); //проверить дублир.
                //    }
                //}
                //else
                //{
                //    DataStatic.policy = new Policy { Name = DataStatic.rule.Name, Descr = DataStatic.rule.Descr };
                //    if (DataStatic.policy.RulesList == null) DataStatic.policy.RulesList = new List<Rule>();
                //    DataStatic.policy.RulesList.Add(DataStatic.rule); //new
                //    DataStatic.PolicesList.Add(DataStatic.policy);
                //}
                    //
            listView.ItemsSource = null;
            listView.Items.Clear();
            List<PolicysForLW> politics = new List<PolicysForLW>();
            foreach (var item in DataStatic.PolicesList)
            {
                politics.Add(new PolicysForLW { Name = item.Name, Count = item.ruleone.CriteriaList.Count });
            }
            listView.ItemsSource = politics;
            }
            catch (Exception)
            {
                
            }
            //серелизация
            string json = JsonConvert.SerializeObject(DataStatic.PolicesList);
            string dir = Directory.GetCurrentDirectory();
            Thread.Sleep(100);
            File.WriteAllText(dir + "\\userdata.db", json);
        }

        private void edit_button_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_выберите_необходимы);
                return;
            }
            //DataStatic.rule = DataStatic.PolicesList[listView.SelectedIndex].RulesList[0];
            CrWindow win = new CrWindow(listView.SelectedIndex, true, winmain);
            ((CrNewWindows)winmain).textBlock1.Text = ExchangeRulesGen.Properties.Resources.Политика_Правило;
            ((CrNewWindows)winmain).groupBox.Header = ExchangeRulesGen.Properties.Resources.Политика_Правило;
            ((CrNewWindows)winmain).separator1.Margin = new Thickness(175, 110, 10, 0);
            NavigationService.Navigate(win);
            //MainWindow win = new MainWindow(DataStatic.PolicesList[listView.SelectedIndex].Name);
            //win.Closing -= Winmain_Closing;
            //win.Closing += Winmain_Closing;
            //win.ShowDialog();

        }

        private void button1_delete_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_выберите_необходимы);
                return;
            }
            DataStatic.PolicesList.RemoveAt(listView.SelectedIndex);
            Winmain_Closing(sender, new System.ComponentModel.CancelEventArgs());
            listView.Items.Refresh();
        }

        private void button_exportps_Click(object sender, RoutedEventArgs e)
        {

            if (listView.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_выберите_необходимы0);
                return;
            }
            if (Properties.Settings.Default.PublisherName == "" || Properties.Settings.Default.PublisherId == "")
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_заполните_данные_в_);
                return;
            }

            ExportPolicy();

        }

        private void button_exportps_Loaded(object sender, RoutedEventArgs e)
        {
            Winmain_Closing(sender, new System.ComponentModel.CancelEventArgs());
        }

        private void listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            edit_button_Click(sender, e);
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (firstStart)
            {
                if (Properties.Settings.Default.PublisherId == "")
                {
                    Properties.Settings.Default.PublisherId = Func.GenGUID();
                    Properties.Settings.Default.Save();
                }
                try
                {

                string dir = Directory.GetCurrentDirectory();
                string json = File.ReadAllText(dir + "\\userdata.db");
                var plist = JsonConvert.DeserializeObject<List<Policy>>(json);             
                DataStatic.PolicesList = plist;

                    List<PolicysForLW> politics = new List<PolicysForLW>();
                    foreach (var item in DataStatic.PolicesList)
                    {
                        politics.Add(new PolicysForLW { Name = item.Name, Count = item.ruleone.CriteriaList.Count });
                    }
                    listView.ItemsSource = politics;

                }
                catch (Exception)
                {

                }
                firstStart = false;
            }

            Winmain_Closing(sender, new System.ComponentModel.CancelEventArgs());

        }

    }
}
