﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        string policyName;
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(string name)
        {
            InitializeComponent();
            this.policyName = name;
            nameTB.Text = policyName;
            // DataStatic.policy = new Policy { Name = policyName };
        }

        private void Create_button_Click(object sender, RoutedEventArgs e)
        {
            //пока сделаем ограничение
            if (listView.Items.Count == 1)
            {
                DialogManager.ShowMessageAsync(this,
                    "Ошибка", "В данной версии на политку может быть только одно правило! Ждите обновлений...");
                return;
            }
            CrWindow win = new CrWindow();
            //CrNewWindows win = new CrNewWindows();
            //win.Closing -= Win_Closing;
            //win.Closing += Win_Closing;
            //win.ShowDialog();

        }

        private void Win_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //    if (DataStatic.policy.RulesList == null)
            //        DataStatic.policy.RulesList = new List<Rule>();
            //        if (DataStatic.rule != null)
            //        {
            //            DataStatic.policy.RulesList.Add(DataStatic.rule);
            //            DataStatic.rule = null;
            //        }
            //    //if (DataStatic.policy.RulesList.Count > 0)
            //    //{
            //        try
            //        {
            //        listView.Items.Clear();
            //        }
            //        catch (Exception)
            //        {
            //        }
            //    try
            //    {
            //        listView.ItemsSource = null;
            //    }
            //    catch (Exception)
            //    {
            //    }
            //    List<string> names = new List<string>();
            //        foreach (var item in DataStatic.policy.RulesList)
            //        {
            //            names.Add(item.Name);
            //        }
            //        listView.ItemsSource = names;
            //   // }
        }

        private void button_gen_test_Click(object sender, RoutedEventArgs e)
        {
        //    DialogManager.ShowMessageAsync(this,
        //            "Ошибка", "Метод удален!");
        }

        private void button_change_Click(object sender, RoutedEventArgs e)
        {
        //    if (listView.SelectedIndex == -1)
        //    {
        //        DialogManager.ShowMessageAsync(this,
        //            "Ошибка", "Для начала выберите необходимое правило!");
        //        return;
        //    }
        //    //CrWindow win = new CrWindow(DataStatic.policy.RulesList[listView.SelectedIndex], true);
        //    CrNewWindows win = new CrNewWindows();
        //    win.Closing -= Win_Closing;
        //    win.Closing += Win_Closing;
        //    win.ShowDialog();
            }

        }

        private void button_delete_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(this,
                    "Ошибка", "Для начала выберите необходимое правило!");
                return;
            }
          //  DataStatic.policy.RulesList.RemoveAt(listView.SelectedIndex);
            Win_Closing(sender, new System.ComponentModel.CancelEventArgs());
            listView.Items.Refresh();
        }

        private void button_done_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
