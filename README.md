# Exchange DLP template generator #


### Ru ###

Программа предназначена для простого и интуитивно понятного создания политик и правил для выявления утечек конфиденциальной информации в корпоративной почте Microsoft Exchange Server.
скачивание доступно в [загрузках](https://bitbucket.org/alexeygritsenko/exchangerulesgen/downloads)
### En ###

The program is designed for easy and intuitive creation of DLP policies and rules to detect leaks of confidential information in Microsoft Exchange Server and Office 365 (Exchange Online).
download release [here](https://bitbucket.org/alexeygritsenko/exchangerulesgen/downloads)