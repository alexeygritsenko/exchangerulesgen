﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRulesGen
{
    static class Func
    {
        public static string GenGUID()
        {
            string giudsharp = Guid.NewGuid().ToString();
            return giudsharp;
        }

        public static string GenGUIDPS()
        {
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                // this script has a sleep in it to simulate a long running script
                //PowerShellInstance.AddScript("start-sleep -s 7; get-service");
                PowerShellInstance.AddScript("[guid]::NewGuid()");
                //[guid]::NewGuid()

                foreach (PSObject result2 in PowerShellInstance.Invoke())
                {
                    string giud = result2.ToString();
                    return giud;
                }
                Console.WriteLine("Finished!");
                return "";
            }
        }

        public static string PSCommand(string com)
        {
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                // this script has a sleep in it to simulate a long running script
                //PowerShellInstance.AddScript("start-sleep -s 7; get-service");
                PowerShellInstance.AddScript(com);
                //[guid]::NewGuid()

                foreach (PSObject result2 in PowerShellInstance.Invoke())
                {
                    string res = result2.ToString();
                    return res;
                }
                Console.WriteLine("Finished!");
                return "";
            }
        }


        public static string PSCommandOrScriptFromCmd(string arg)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = @"powershell.exe";
            startInfo.Arguments = arg;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();

            string output = "";
            string errors = "";
            try
            {            
             output = process.StandardOutput.ReadToEnd();
             errors = process.StandardError.ReadToEnd();
            }
            catch (Exception e1)
            {
                System.Windows.MessageBox.Show("Исключение при выполнения ПШ скрипта: " + e1.ToString());
                throw;
            }

            if (!string.IsNullOrEmpty(errors)) return errors;
            return output;
        }


        public static string GenXmlForOneRule(Rule rule, string pathpolicyfolder, string polName = "polName", string polDescr = "polDescr")
        {

            

            string RulePackId = Func.GenGUID(); //"f14f244b-d9c0-4e67-8a8b-e4816dacf17a";
            var PublisherId = "d3fcaa44-d800-41e3-9adc-92005ece4f08";
            var langcode = "ru-ru";
            var EntityId = Func.GenGUID(); //"353a906d-088f-4d45-8544-97e04c92c4d0";
            var confLevel = 70;
            var ResourceIdRef = EntityId; //"353a906d-088f-4d45-8544-97e04c92c4d0";
            //
            var CriteriaS = rule.CriteriaList;
            string mathes = "";
            string mathesRegex = "";
            string Keywords = "";
            for (int i = 0; i <= CriteriaS.Count - 1; i++)
            {
                if (CriteriaS[i].TypeS == "число")
                {
                    if (i == 0)
                        mathes += @"<IdMatch idRef = ""RegexMatchNum" + i + @"""/>" + Environment.NewLine;
                    else
                        mathes += @"<Match idRef = ""RegexMatchNum" + i + @"""/>" + Environment.NewLine;
                    mathesRegex += @"<Regex id = ""RegexMatchNum" + i + @""">" + CriteriaS[i].Regex + @"</Regex>" + Environment.NewLine;
                }
                if (CriteriaS[i].TypeS == "слово")
                {


                    //mathesRegex += @"<Regex id = ""KeywordMatchNum" + i + @""">" + CriteriaS[i].Regex + @"</Regex>" + Environment.NewLine;
                    if (Keywords == "")
                    {
                        Keywords = @"<Keyword id=""KeywordMatchNum" + i + @""" ><Group matchStyle = ""word"">";
                        if (i == 0)
                            mathes += @"<IdMatch idRef = ""KeywordMatchNum" + i + @"""/>" + Environment.NewLine;
                        else
                            mathes += @"<Match idRef = ""KeywordMatchNum" + i + @"""/>" + Environment.NewLine;
                    }

                    Keywords += "<Term>" + CriteriaS[i].WordForTwoType + "</Term>";
                }

            }
            if (Keywords.Contains("Term"))
                Keywords += "</Group></Keyword>";
            else
                Keywords = "";
            //
            string statxml = Properties.Resources.templ1;
            statxml = statxml;
            //var faf = string.Format("Password: {0} and {0}", "vasya");
            //faf = faf;
            string ruleDescrLocal = rule.Descr;
            if (string.IsNullOrEmpty(ruleDescrLocal)) ruleDescrLocal = "none";
            if (string.IsNullOrEmpty(polDescr)) polDescr = "none";
            string xml2 = string.Format(statxml, RulePackId, PublisherId, "AlexeyAndAnton", polName, polDescr, EntityId, confLevel, mathes, mathesRegex + Keywords, ResourceIdRef, langcode, rule.Name, ruleDescrLocal);
            xml2 = xml2;
            string namefile = "rule_" + DateTime.Now.ToString("dd_MM_yyyy-hh_mm_ss") + ".xml";
            string filenamefullpath = pathpolicyfolder + "\\" + namefile;
            //File.WriteAllText(filenamefullpath, xml2, Encoding.Unicode);

            return filenamefullpath;
        }


        public static string GenXmlForOneRuleTwo(Rule rule, string pathpolicyfolder, string datetime, string polName = "polName", string polDescr = "polDescr")
        {



            string RulePackId = Func.GenGUID(); //"f14f244b-d9c0-4e67-8a8b-e4816dacf17a";
            var PublisherId = "d3fcaa44-d800-41e3-9adc-92005ece4f08";
            var langcode = "ru-ru";
            var EntityId = Func.GenGUID(); //"353a906d-088f-4d45-8544-97e04c92c4d0";
            var confLevel = 70;
            var ResourceIdRef = EntityId; //"353a906d-088f-4d45-8544-97e04c92c4d0";
            //
            var CriteriaS = rule.CriteriaList;
            //пров. ключевое
            Criteria critMain = null;
            for (int i = 0; i <= CriteriaS.Count - 1; i++)
            {
                if (CriteriaS[i].Mainkey)
                {
                    critMain = CriteriaS[i];
                    CriteriaS.RemoveAt(i);
                    break;
                }
            }
            if (critMain != null)
            {
                List<Criteria> critListNew = new List<Criteria>();
                critListNew.Add(critMain);
                critListNew.AddRange(CriteriaS);
                CriteriaS = critListNew;
            }
            //пров. ключевое

            string mathes = "";
            string mathesRegex = "";
            string Keywords = "";
            for (int i = 0; i <= CriteriaS.Count - 1; i++)
            {
                if (CriteriaS[i].TypeS == "число")
                {
                    if (i == 0)
                        mathes += @"<IdMatch idRef = ""RegexMatchNum" + i + @"""/>" + Environment.NewLine;
                    else
                        mathes += @"<Match idRef = ""RegexMatchNum" + i + @"""/>" + Environment.NewLine;
                    mathesRegex += @"<Regex id = ""RegexMatchNum" + i + @""">" + CriteriaS[i].Regex + @"</Regex>" + Environment.NewLine;
                }
                if (CriteriaS[i].TypeS == "слово" || CriteriaS[i].TypeS == "словарь")
                {


                    //mathesRegex += @"<Regex id = ""KeywordMatchNum" + i + @""">" + CriteriaS[i].Regex + @"</Regex>" + Environment.NewLine;
                    if (Keywords == "")
                    {
                        Keywords = @"<Keyword id=""KeywordMatchNum" + i + @""" ><Group matchStyle = ""word"">";
                        if (i == 0)
                            mathes += @"<IdMatch idRef = ""KeywordMatchNum" + i + @"""/>" + Environment.NewLine;
                        else
                            mathes += @"<Match idRef = ""KeywordMatchNum" + i + @"""/>" + Environment.NewLine;
                    }

                    if (CriteriaS[i].TypeS == "слово")
                    Keywords += "<Term>" + CriteriaS[i].WordForTwoType + "</Term>";

                    if (CriteriaS[i].TypeS == "словарь")
                    {
                        foreach (var item in CriteriaS[i].Dic)
                        {
                            Keywords += "<Term>" + item + "</Term>";
                        }
                        
                    }
                }

            }
            if (Keywords.Contains("Term"))
                Keywords += "</Group></Keyword>";
            else
                Keywords = "";
            //
            string statxml = Properties.Resources.templ1;
            statxml = statxml;
            //var faf = string.Format("Password: {0} and {0}", "vasya");
            //faf = faf;
           // string ruleDescrLocal = rule.Descr;
           // if (string.IsNullOrEmpty(ruleDescrLocal)) ruleDescrLocal = "none";
            if (string.IsNullOrEmpty(polDescr)) polDescr = "none";
            string xml2 = string.Format(statxml, RulePackId, Properties.Settings.Default.PublisherId, Properties.Settings.Default.PublisherName, polName + "_" + datetime, polDescr, EntityId, confLevel, mathes, mathesRegex + Keywords, ResourceIdRef, langcode, polName, polDescr);
            xml2 = xml2;
            //string namefile = "rule_" + DateTime.Now.ToString("dd_MM_yyyy-hh_mm_ss") + ".xml";
            //string filenamefullpath = pathpolicyfolder + "\\" + namefile;
            //File.WriteAllText(filenamefullpath, xml2, Encoding.Unicode);

            return xml2;
        }


        public static string GenXmlFordlpPolicyTemplatesWithOneRule(Rule rule, string pathpolicyfolder, string datetime, string polName = "polName", string polDescr = "polDescr")
        {
            string xmlrule = Func.GenXmlForOneRuleTwo(rule, pathpolicyfolder, datetime, polName, polDescr);
            string polxml = Properties.Resources.policytemplate3;

            polxml = polxml.Replace("{0}", Func.GenGUID());
            polxml = polxml.Replace("{1}", Properties.Settings.Default.PublisherName);
            polxml = polxml.Replace("{2}", polName);
            polxml = polxml.Replace("{3}", polName + "_" + datetime);
            polxml = polxml.Replace("{4}", xmlrule);

            //string namefile = "policy_and_rule_" + DateTime.Now.ToString("dd_MM_yyyy-hh_mm_ss") + ".xml";
            //string filenamefullpath = pathpolicyfolder + "\\" + namefile;
            File.WriteAllText(pathpolicyfolder, polxml);
            return pathpolicyfolder;
        }



    }


}
