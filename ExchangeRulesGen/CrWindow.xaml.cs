﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ExchangeRulesGen
{
    /// <summary>
    /// Логика взаимодействия для CrWindow.xaml
    /// </summary>
    public partial class CrWindow : Page
    {
        Policy pol = null;
        //Rule rule = null;
        bool edit = false;
        MetroWindow winmain = null;
        int polIndex = -1;

        public CrWindow()
        {
            InitializeComponent();
            pol = new Policy();
        }

        public CrWindow(MetroWindow winmain)
        {
            InitializeComponent();
            this.winmain = winmain;
            pol = new Policy();
        }

        public CrWindow(int polIndex, bool edit, MetroWindow winmain)
        {
            InitializeComponent();
            this.pol = DataStatic.PolicesList[polIndex];
            this.polIndex = polIndex;
            nameTB.Text = pol.ruleone.Name;
            DescrTB.Text = pol.ruleone.Descr;
            ListView1.ItemsSource = pol.ruleone.CriteriaList;
            this.edit = edit;
            this.winmain = winmain;
        }



        private void edit_button_Click(object sender, RoutedEventArgs e)
        {
            //Criteria cr = new Criteria { Name = "крит1", TypeS = "Слово", Mainkey="mket"};
            //List<Criteria> list1 = new List<Criteria>();
            //list1.Add(new Criteria() { Name = "John Doe", TypeS = "42", Mainkey = "john@doe-family.com" });

            //list1.Add(cr);
            ////ListView1.ItemsSource = null; //обязательно! без него не обновится(
            //ListView1.ItemsSource = list1;
            if (ListView1.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_выберите_необходимы);
                return;
            }
            Step2Window win2 = new Step2Window(ref pol.ruleone, ListView1.SelectedIndex);
            //CrNewWindows win = new CrNewWindows();
            win2.Closing -= St2_Closing;
            win2.Closing += St2_Closing;
            win2.ShowDialog();
            


        }

     
        private void Create_button_Cr_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(nameTB.Text))
            {
                //сообщить

                //MessageBox.Show("Для начала заполните как минимум Наименование!"); //не красиво((
                DialogManager.ShowMessageAsync(winmain,
                ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_заполните_как_миним);
                

                return;
            }


            if (pol.ruleone == null)
            {
                pol.ruleone = new Rule { Name = nameTB.Text, Descr = DescrTB.Text };
                pol.Name = nameTB.Text;
                pol.Descr = DescrTB.Text;
            }
            Step2Window st2 = new Step2Window(ref pol.ruleone);
            st2.Closing -= St2_Closing;
            st2.Closing += St2_Closing;
            st2.ShowDialog();
            
        }

        private void St2_StateChanged(object sender, EventArgs e)
        {
            var faf = ((Step2Window)sender).WindowState;
        }

        private void St2_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //if (DataStatic.rule == null) return;
            //if (DataStatic.rule.CriteriaList == null) return;
            //if (!edit)
            //{
            //    var pol = new Policy { Name = rule.Name, Descr = rule.Descr };
            //    pol.RulesList = new List<Rule>();
            //    pol.RulesList.Add(DataStatic.rule);
            //    //DataStatic.PolicesList.Add();
            //    edit = true;
            //}
            //else
            //{

            //}
            ListView1.ItemsSource = null;
            ListView1.Items.Clear();            
            ListView1.ItemsSource = pol.ruleone.CriteriaList;
            if (polIndex != -1)
                DataStatic.PolicesList[polIndex] = pol;
            else
            {
                DataStatic.PolicesList.Add(pol);
                polIndex = DataStatic.PolicesList.IndexOf(pol);
            }
        }

        private void button_done_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
        }

        private void button_cancel_Click(object sender, RoutedEventArgs e)
        {
            //if (!edit)
            //DataStatic.rule = null;
            //this.Close();
        }

        private void button1_delete_Click(object sender, RoutedEventArgs e)
        {
            if (ListView1.SelectedIndex == -1)
            {
                DialogManager.ShowMessageAsync(winmain,
                    ExchangeRulesGen.Properties.Resources.Ошибка, ExchangeRulesGen.Properties.Resources.Для_начала_выберите_необходимы);
                return;
            }
            pol.ruleone.CriteriaList.RemoveAt(ListView1.SelectedIndex);
            St2_Closing(sender, new System.ComponentModel.CancelEventArgs());
            ListView1.Items.Refresh();
        }

        private void ListView1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            edit_button_Click(sender, e);

        }

        private void ListView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int text = 0;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var aaff = ((System.Windows.FrameworkElement)sender).DataContext;
            //aaff = aaff;
            for (int i = 0; i <= pol.ruleone.CriteriaList.Count - 1; i++)
            {
                if (pol.ruleone.CriteriaList[i].Name == ((Criteria)aaff).Name)
                    pol.ruleone.CriteriaList[i].Mainkey = true;
                else pol.ruleone.CriteriaList[i].Mainkey = false;
            }
            St2_Closing(sender, new System.ComponentModel.CancelEventArgs());
            ListView1.Items.Refresh();
            //
            //ListView1.SelectedItem.
        }

        private void ok_button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(nameTB.Text) || pol.ruleone == null)
            {
                ((CrNewWindows)winmain).frame.GoBack();
                return;
            }

            //имя сначала применить)
            pol.Name = nameTB.Text;
            pol.ruleone.Name = nameTB.Text;
            //
            ((CrNewWindows)winmain).textBlock1.Text = ExchangeRulesGen.Properties.Resources.Список_политик;
            ((CrNewWindows)winmain).groupBox.Header = ExchangeRulesGen.Properties.Resources.Список_политик;
            ((CrNewWindows)winmain).separator1.Margin = new Thickness(155, 110, 10, 0);
            //((CrNewWindows)winmain).PART_BackButton.IsEnabled = false;
            ((CrNewWindows)winmain).frame.GoBack();
            //NavigationService
        }
    }
}
